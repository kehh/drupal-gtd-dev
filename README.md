# Drupal Global Training Day setup

## Quick start

1. You need to install [docker](https://docs.docker.com/install/) and [docker compose](https://docs.docker.com/compose/install/).
  
  You should be able to verify installed software 
  by opening terminal (CLI) on your machine and running
  
  ```
  $ docker -v
  Docker version 18.06.0-ce, build e68fc7a
  $ docker-compose -v
  docker-compose version 1.22.0, build f46880f
  ```
  
  If you can't see the versions, try to reinstall prerequisites.
  
2. Clone [current repository](https://gitlab.com/testudio/drupal-gtd-dev/)
  
  ```
  git clone git@gitlab.com:testudio/drupal-gtd-dev.git
  ```
  
3. Initialise containers

  ```
  cd drupal-gtd-dev
  docker-compose up -d --build
  docker exec -it webgtd composer install
  ```
 
  
  You can now access [website](http://0.0.0.0:6060) and [phpMyAdmin](http://0.0.0.0:7070).
  
  Database credentials can be found in `docker-compose.yml` file.
  
  For more information see [documentation](./docs/README.md).
  
4. Installing Drupal

Follow future steps in [documentation](./docs/INSTRUCTIONS_DEV.md).
  
## Debugging
  
Verify that all containers are running. Command ``

```
docker-compose ps
```

should return

```
   Name                 Command               State               Ports             
------------------------------------------------------------------------------------
mysql        docker-entrypoint.sh mysqld      Up      3306/tcp, 33060/tcp           
phpmyadmin   /run.sh supervisord -n           Up      0.0.0.0:7070->80/tcp, 9000/tcp
webgtd       docker-php-entrypoint apac ...   Up      0.0.0.0:6060->80/tcp   
```

`docker-compose logs` should give you more insight if you have any issues.

## Cleanup

```
docker-compose stop
docker-compose rm
docker images # to cleanup required images with `docker rmi [id]`
```

## Troubleshooting 

### Windows Home

Dicker is not available on Windows 10 Home Edition operating system. 

### 300 seconds timeout

If you encounter `... exceeded the timeout of 300 seconds` type
of error you can always set `export COMPOSER_PROCESS_TIMEOUT=600`
to increase composer timeout, e.g. 

```
docker exec -it webgtd bash -c "export COMPOSER_PROCESS_TIMEOUT=600 && composer install"
```

## Links

* [Drupal Global Training Day](https://groups.drupal.org/node/512931)
* [Drupal](https://www.drupal.org/)
