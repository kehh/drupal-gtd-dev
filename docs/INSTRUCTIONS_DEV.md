# `Drupal 8 for PHP developers` installation instructions

## Prerequisites

Read main [README](../README.md)

## 1. Install Drupal

1. Install Drupal website by running

```
docker exec -it webgtd composer install
docker exec -it webgtd composer site-install-docker
```

## 2. Create Gitlab account.

1. Create [Gitlab](https://www.gitlab.com/) account if you don't have one.
2. [Generate SSH key](https://docs.gitlab.com/ee/ssh/README.html#generating-a-new-ssh-key-pair) if you haven't done so before and [add public key to Gitlab account](https://docs.gitlab.com/ee/ssh/README.html#adding-a-ssh-key-to-your-gitlab-account).
3. [Create GitHub repository](https://docs.gitlab.com/ee/gitlab-basics/create-project.html) called `drupal-training-201811`

## Optional. Continuous delivery.

This step is not required for training but there would be tutorial 
so if you would like to do hands on session, follow the instructions below.

1. Create Platfrom.sh account
  * Click `Start free trial` on [Platfrom.sh home pgae](https://platform.sh/) and register your account
  * Create environment with Drupal 8 installed.
2. [Generate SSH key](https://docs.gitlab.com/ee/ssh/README.html#generating-a-new-ssh-key-pair) if you haven't done so before in th previous step and [add public key to Platform account](https://docs.platform.sh/development/ssh.html).
