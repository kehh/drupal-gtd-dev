# `Drupal 8 intermediate training` homework

## Prerequisites

You would receive email about how to login to Drupal.

Complete the following tasks before the training.

## 1. Understanding content types, taxonomy and fields.

After you login to Drupal, do the following:

* Create new content type called `Practice [Your Name]`. Replace value in brackets. [Read more about content types](https://www.drupal.org/docs/user_guide/en/structure-content-type.html).

    If you need inspiration, you can create `Movie` content type.

* Add 3 customs fields to above content type. You can add any 3 fields but make sure they are different types. [Read more about custom fields](https://www.drupal.org/docs/user_guide/en/structure-fields.html).

    If you need inspiration, you can create `Release date`, `Poster` and `Tagline` fields.

* Add taxonomy vocabulary and add more than 3 values. [Read more about taxonomy](https://www.drupal.org/docs/user_guide/en/structure-taxonomy-setup.html).

    If you need inspiration, you can create `Rating` vocabulary with the following values: `G`, `PG`, `M`, `MA`, `R`.

* Add taxonomy reference field to your content type. [Read more about adding reference field](https://www.drupal.org/docs/user_guide/en/structure-adding-reference.html).

* Create 5 to 10 nodes of your content type. [Read more about creating content](https://www.drupal.org/docs/user_guide/en/content-create.html).

* Change the order of the fields on editing form, change your taxonomy reference field to display as checkboxes / radio buttons. [Read more about form modes](https://www.drupal.org/docs/user_guide/en/structure-form-editing.html).

* Modify the order of items displayed on your content type page, hide one label, display one label as inline. [Read more about view modes](https://www.drupal.org/docs/user_guide/en/structure-content-display.html).

## 2. Understanding blocks.

* Create custom block with your name. [Read more about blocks](https://www.drupal.org/docs/user_guide/en/block-create-custom.html).

* Place your block to the footer regio of website. [Read more about placing blocks](https://www.drupal.org/docs/user_guide/en/block-place.html).

## 3. Understanding basic views data reporting.

* Create view to display all items of your content type you created. Display the view as a table, order it by title. [Read more about views](https://www.drupal.org/docs/user_guide/en/views-create.html).
